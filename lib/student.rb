$LOAD_PATH << '.'

require 'course'

class Student
  attr_accessor :first_name, :last_name, :registered_courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @registered_courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def courses
    @registered_courses
  end

  def enroll(course_name)
    raise if @registered_courses.any? {|course| course.conflicts_with?(course_name)}
    @registered_courses << course_name unless @registered_courses.include?(course_name)
    course_name.students << self

  end

  def course_load
    course_info = Hash.new(0)
    @registered_courses.each do |course|
      course_info[course.department] += course.credits
    end
    course_info
  end

end
